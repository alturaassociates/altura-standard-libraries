# Altura Standard Libraries #
Altura has developed a centralized approach to modeling modern building analytics systems that is designed to enforce project-to-project tagging consistency across a diversity of building portfolios and external data providers. An efficient deployment of analytics rules requires consistent tagging of point objects for queries based on a unique set of tags defining the point’s multiple characteristics.

The tagging schema is primarily based on the metamodel developed by the Project Haystack initiative, but has expanded the library of underlying tags to include additional tags for attributes often found in building systems not yet included in the Project Haystack tag set. The scope of standard names and tags has been built around completing real world-building analytics integrations for building automation systems, metering, commissioning, optimization, measurement and verification, and building management tools.

## Standard Point Name Library ##
The **Altura Point Standard Name Library** is a basis for normalizing point metadata, which can be used to reliably map each relevant tag to a database point object by assigning it a standard name. With the appropriate tools, this open library allows integrators modeling analytics databases to in a single step select a standard point name from the library and reliably apply all associated tags.

This library associates a standard point name (stdName) with a set of tags to enable faster normalization and haystack modeling of incoming remote system data. The following components are included in the Altura Point Standard Name Library grid:

* **tagSet** Primary marker tag used for identifying the library record.
* **tags** Comma-separated list of tags modeling the point entity, to be applied as marker tags to the point objects mapped to the library record.
* **stdName** Display name for the library record, to be applied as the navName of the point objects mapped to the library record.
* **description** Technical description of the library record and associated point objects.
* **kindType** “Number” or “Bool/Str” record type.
* **tagsType** Required tags “point” and the appropriate identifier: “sensor”, “sp”, or “cmd”.
* **effTags** Identifies “effective” setpoints or sensors (integral to process control) as well as “generic” setpoints or sensors.
* **unitDefault** The expected unit of the point object, if applicable.
* **group** String representing groupings for like library records.
* **disMacro** The display configuration for the library record.

## Standard Tag Library 

The Altura Standard Tag Library composes a list of all specific metadata tags used throughout Altura’s libraries (point name, prefix, equip type, equip features). The semantic data model incorporates the individual terms (tags) defined in the open-source Project Haystack initiative, as well as extensions of that model created by Altura to comprehensively tag building information systems. The following components are included in the Altura Standard Tag Library grid:

* **stdTag** Primary marker tag used for identifying the library record.
* **tagName** Display name for the library record, applied as a marker tag for all associated library references.
* **description** Technical description of the library record and associated point objects.
* **source** Entity responsible for defining the semantic tag (Project Haystack or Altura).
* **stdLibReference** Applicable Altura standard library reference for which the tag is applied to one or more library records.